def reverse(inputfilename,outputfilename):
	lines = open(inputfilename,'r',encoding = 'utf-8').readlines()
	f = open(outputfilename,'a+',encoding = 'utf-8')
	for line in lines:
		line = line.strip().split(" ")
		line.reverse()
		f.write(' '.join(line)+'\n')
	f.close()

reverse('x.txt','x_r.txt')
reverse('y.txt','y_r.txt')
print('finished')
