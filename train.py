import tensorflow as tf
import data_untils
import numpy as np
import datetime
import model


hidden_layer_size = 128
depth = 2
source_vocabulary_size = len(data_untils.tokenizer) + 1
target_vocabulary_size = len(data_untils.tokenizer) + 1
embedding_size = 128
grad_clip = 1.0
max_sequence_length = 20
learning_rate = 1e-3
batch_size = 128
beam_width = 12
num_epochs = 15
allow_soft_placement = True
log_device_placement = False


def main():
	xfilename = 'x_r.txt'
	yfilename = 'y_r.txt'
	train,dev = data_untils.data_iter(xfilename,yfilename)
	seq2seqmodel = model.Seq2SeqModel(hidden_layer_size = hidden_layer_size,
        depth = depth,
        source_vocabulary_size = source_vocabulary_size,
        target_vocabulary_size = target_vocabulary_size,
        embedding_size = embedding_size,
        grad_clip = grad_clip,
        max_sequence_length = max_sequence_length,
        learning_rate = learning_rate,
        batch_size = batch_size,
        beam_width = beam_width,
        target_letter_to_int = data_untils.tokenizer)
	evaluate_step = int((len(train['source_sequence'])-1)/batch_size) + 1
	# print(evaluate_step)
	session_conf = tf.ConfigProto(allow_soft_placement=allow_soft_placement,log_device_placement=log_device_placement)
	with tf.Session(config=session_conf) as sess:
		sess.run(tf.global_variables_initializer())
		traindata = list(zip(train['source_sequence'],train['target_sequence'],train['ground_truth'],train['length']))
		batches = data_untils.batch_iter(traindata, batch_size, num_epochs)
		result = []
		for batch in batches:
			loss,step = seq2seqmodel.train(batch,sess)
			time_str = datetime.datetime.now().isoformat()
			print(time_str ,'step:',step,'loss:',loss)
			if step % evaluate_step == 0:
				print('evaluate')
				evaluate = []
				devdata = list(zip(dev['source_sequence'],dev['length'],dev['rhyme_index']))
				dev_batches = data_untils.batch_iter(devdata, batch_size,1,False)
				for dev_batch in dev_batches:
					# print(dev_batch)
					predict = seq2seqmodel.eval(dev_batch,sess)
					evaluate.append(predict)
				# print(len(evaluate))
				save_name = 'evaluate' + str(step) + '.npy'
				np.save(save_name,evaluate)
				print('evaluate save at:',save_name)
	# 	result.append(evaluate)
	# 	print(len(evaluate))
	# print(len(result))
	# np.save('predict.npy',result)
main()


