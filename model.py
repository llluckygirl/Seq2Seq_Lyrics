import tensorflow as tf
import data_untils
from tensorflow.python.layers.core import Dense

# hidden_layer_size = 128
# depth = 2
# source_vocabulary_size = len(data_untils.tokenizer) 
# target_vocabulary_size = len(data_untils.tokenizer)
# embedding_size = 128
# grad_clip = 1.0
# max_sequence_length = 20
# learning_rate = 1e-3
# batch_size = 2
# beam_width = 5

class Seq2SeqModel(object):
    def __init__(self,
        hidden_layer_size,
        depth,
        source_vocabulary_size,
        target_vocabulary_size,
        embedding_size,
        grad_clip,
        max_sequence_length,
        learning_rate,
        batch_size,
        beam_width,
        target_letter_to_int,
        is_inference = False):

        self.hidden_layer_size = hidden_layer_size
        self.depth = depth
        self.source_vocabulary_size = source_vocabulary_size
        self.target_vocabulary_size = target_vocabulary_size
        self.grad_clip = grad_clip
        self.embedding_size = embedding_size
        self.max_sequence_length = max_sequence_length
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.target_letter_to_int = target_letter_to_int
        self.beam_width = beam_width
        self.global_step = tf.Variable(0, name="global_step", trainable=False)
        self.build_model()

    def build_model(self):
        print('building model...')

        self.get_inputs()
        self.get_encoder_layer()
        self.get_decoder_layer()

        # Merge all the training summaries
        self.summary_op = tf.summary.merge_all()


    def get_inputs(self):
        '''
        模型输入tensor
        '''
        self.inputs = tf.placeholder(tf.int32, [None, None], name='inputs')
        self.targets = tf.placeholder(tf.int32, [None, None], name='targets')
        self.ground = tf.placeholder(tf.int32, [None, None], name='ground')
        # rhyme_index
        self.rhyme = tf.placeholder(tf.int64,[None,None],name = 'rhyme')
        
        # 定义target序列最大长度（之后target_sequence_length和source_sequence_length会作为feed_dict的参数）
        self.target_sequence_length = tf.placeholder(tf.int32, (None,), name='target_sequence_length')
        self.source_sequence_length = tf.placeholder(tf.int32, (None,), name='source_sequence_length')

    def get_encoder_layer(self):

        '''
        构造Encoder层
        
        '''
        # Encoder embedding
        encoder_embed_input = tf.contrib.layers.embed_sequence(self.inputs, self.source_vocabulary_size, self.embedding_size)

        # RNN cell
        def get_lstm_cell(rnn_size):
            lstm_cell = tf.contrib.rnn.LSTMCell(rnn_size, initializer=tf.random_uniform_initializer(-0.1, 0.1, seed=2))
            return lstm_cell

        cell = tf.contrib.rnn.MultiRNNCell([get_lstm_cell(self.hidden_layer_size) for _ in range(self.depth)])
        
        self.encoder_output, self.encoder_state = tf.nn.dynamic_rnn(cell, encoder_embed_input, 
                                                          sequence_length=self.source_sequence_length, dtype=tf.float32)

    def get_decoder_layer(self):
        '''
        构造Decoder层
        
        '''
        # 1. Embedding
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        decoder_embeddings = tf.Variable(tf.random_uniform([self.target_vocabulary_size, self.embedding_size]))
        self.decoder_embed_input = tf.nn.embedding_lookup(decoder_embeddings, self.ground)

        # 2. 构造Decoder中的RNN单元
        def get_decoder_cell(rnn_size):
            decoder_cell = tf.contrib.rnn.LSTMCell(rnn_size,
                                               initializer=tf.random_uniform_initializer(-0.1, 0.1, seed=2))
            return decoder_cell
        cell = tf.contrib.rnn.MultiRNNCell([get_decoder_cell(self.hidden_layer_size) for _ in range(self.depth)])
         
        # 3. Output全连接层
        output_layer = Dense(self.target_vocabulary_size,
                            use_bias=False,
                            kernel_initializer = tf.truncated_normal_initializer(mean = 0.0, stddev=0.1))


        # 4. Training decoder
        with tf.variable_scope("decode"):
            # 得到help对象
            training_helper = tf.contrib.seq2seq.TrainingHelper(inputs=self.decoder_embed_input,
                                                                sequence_length=self.target_sequence_length,
                                                                time_major=False)
            # 构造decoder
            training_decoder = tf.contrib.seq2seq.BasicDecoder(cell,
                                                               training_helper,
                                                               self.encoder_state,
                                                               output_layer) 
            self.training_decoder_output, _,_ = tf.contrib.seq2seq.dynamic_decode(training_decoder,
                                                                           impute_finished=True,
                                                                           maximum_iterations=self.max_sequence_length)
        # 5. Predicting decoder
        # 与training共享参数
        # with tf.variable_scope("decode", reuse=True):
            # 创建一个常量tensor并复制为batch_size的大小
            start_tokens = tf.tile(tf.constant([self.target_letter_to_int['<GO>']], dtype=tf.int32), [self.batch_size], 
                                   name='start_tokens')

            
            # predicting_helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(decoder_embeddings,
            #                                                         start_tokens,
            #                                                         self.target_letter_to_int['<EOS>'])
            # predicting_decoder = tf.contrib.seq2seq.BasicDecoder(cell,
            #                                                 predicting_helper,
            #                                                 self.encoder_state,
            #                                                 output_layer)
            

            # predicting_decoder_beam = tf.contrib.seq2seq.BeamSearchDecoder(cell = cell,
            #                                                    embedding = decoder_embeddings,
            #                                                    start_tokens= start_tokens,
            #                                                    end_token = self.target_letter_to_int['<EOS>'],
            #                                                    # initial_state = self.encoder_state,
            #                                                    initial_state = tf.contrib.seq2seq.tile_batch(self.encoder_state,multiplier = beam_width),
            #                                                    beam_width = self.beam_width,
            #                                                    output_layer = output_layer)
            
            first_inputs = tf.nn.embedding_lookup(decoder_embeddings,start_tokens)
            # print(first_inputs)

            first_outputs, first_states = cell(first_inputs, self.encoder_state)

            self.first_predictions = output_layer(first_outputs)
            # print(self.first_predictions)

            rhyme_tokens = []
            for i in range(self.batch_size):
                rhyme_tokens.append(tf.argmax(self.first_predictions[i,self.rhyme[i,0]:self.rhyme[i,1]]) + self.rhyme[i,0])

            self.rhyme_token = tf.cast(tf.stack(rhyme_tokens),tf.int32)

            predicting_decoder = tf.contrib.seq2seq.BeamSearchDecoder(cell = cell,
                                                               embedding = decoder_embeddings,
                                                               start_tokens= self.rhyme_token,
                                                               end_token = self.target_letter_to_int['<EOS>'],
                                                               # initial_state = self.encoder_state,
                                                               initial_state = tf.contrib.seq2seq.tile_batch(self.encoder_state,multiplier = self.beam_width),
                                                               beam_width = self.beam_width,
                                                               output_layer = output_layer)

            self.predicting_decoder_output,_,_ = tf.contrib.seq2seq.dynamic_decode(predicting_decoder,
                                                                impute_finished=False,
                                                                swap_memory=True,
                                                                maximum_iterations=self.max_sequence_length)

            self.start_token = tf.expand_dims(tf.transpose(tf.tile([self.rhyme_token], [self.beam_width,1]),perm = [1,0]),-1)
            # print(self.start_token)
            self.predict_pre = tf.transpose(self.predicting_decoder_output.predicted_ids, perm = [0,2,1])
            # print(self.predict_pre)
            self.predict = tf.concat([self.start_token,self.predict_pre],-1)
            # print(self.predict)
                                        

        
   
        self.training_logits = tf.identity(self.training_decoder_output.rnn_output, 'logits')

        # self.predicting_logits = tf.identity(self.predicting_decoder_output.rnn_output, 'logits')
        # self.predicting_decoder_pred = tf.argmax(self.predicting_logits, axis=-1,name='decoder_pred_train')
        self.masks = tf.sequence_mask(self.target_sequence_length, self.max_sequence_length, dtype=tf.float32, name='masks')
        with tf.name_scope("optimization"):
        # Loss function
            # print(self.targets)
            # print(self.training_logits)
            self.cost = tf.contrib.seq2seq.sequence_loss(
                self.training_logits,
                self.targets,
                self.masks)

            # Optimizer
            optimizer = tf.train.AdamOptimizer(self.learning_rate)

            # Gradient Clipping
            gradients = optimizer.compute_gradients(self.cost)
            capped_gradients = [(tf.clip_by_value(grad, -5., 5.), var) for grad, var in gradients if grad is not None]
            self.train_op = optimizer.apply_gradients(capped_gradients,global_step=self.global_step)


    def train(self,batch,sess):
        inputs,targets,ground_truth,length = zip(*batch)
        input_feed =  {self.inputs: inputs,
            self.targets: targets,
            self.ground:ground_truth,
            self.target_sequence_length: length,
            self.source_sequence_length: length}

        # print(inputs,targets,ground_truth,length)
        output_feed = [self.train_op,self.cost,self.global_step]

        
        output = sess.run(output_feed,input_feed)

        return output[1],output[2]

    def eval(self,dev_batch,sess):
        inputs,source_sequence_length,rhyme = zip(*dev_batch)
        # print(inputs)
        # print(source_sequence_length)
        # print(rhyme)
        input_feed = {self.inputs:inputs,
        self.source_sequence_length:source_sequence_length,
        self.rhyme:rhyme}


        output_feed = [self.predict]


        predict = sess.run(output_feed,input_feed)
        

        return predict[0]

# def test():


#     xfilename = 'x_r_s.txt'
#     yfilename = 'y_r_s.txt'
#     train_data,dev_data = data_untils.data_iter(xfilename,yfilename)

#     seq2seqmodel = Seq2SeqModel(hidden_layer_size = hidden_layer_size,
#         depth = depth,
#         source_vocabulary_size = source_vocabulary_size,
#         target_vocabulary_size = target_vocabulary_size,
#         embedding_size = embedding_size,
#         grad_clip = grad_clip,
#         max_sequence_length = max_sequence_length,
#         learning_rate = learning_rate,
#         batch_size = batch_size,
#         beam_width = beam_width,
#         target_letter_to_int = data_untils.tokenizer)


#     with tf.Session() as sess:
#         sess.run(tf.global_variables_initializer())



#         predict = seq2seqmodel.eval(dev_data,sess)

#         print(predict)

# test()
        
        
        
        




