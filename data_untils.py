import tensorflow as tf
import numpy as np

token = ['<UNK>','<GO>','<EOS>']
tokenizer = np.load('vocab_dict.npy')[()]
tokenizer['<UNK>'] = len(tokenizer) + 1
tokenizer['<GO>'] = len(tokenizer) + 1
tokenizer['<EOS>'] = len(tokenizer) + 1 
# print(tokenizer['你'])
# values = list(tokenizer.values())
# values.sort()
# print(values)

index = np.load('index.npy')
# print(index)


def load_data(filename):
    lines = open(filename,encoding = 'utf-8').readlines()
    result = []
    for line in lines:
        line = line.strip()
        result.append(line)
    return result

def train_test_split(source_sequence,target_sequence,ground_truth):
	np.random.seed(10)
	shuffle_indices = np.random.permutation(np.arange(len(target_sequence)))
	source_sequence_shuffled = source_sequence[shuffle_indices]
	target_sequence_shuffled = target_sequence[shuffle_indices]
	ground_truth_shuffled = ground_truth[shuffle_indices]


	# Split train/test set
	# TODO: This is very crude, should use cross-validation
	dev_sample_index = -1 * int(0.2 * float(len(target_sequence)))
	source_sequence_train, source_sequence_dev = source_sequence_shuffled[:dev_sample_index], source_sequence_shuffled[dev_sample_index:]
	target_sequence_train, target_sequence_dev = target_sequence_shuffled[:dev_sample_index], target_sequence_shuffled[dev_sample_index:]
	ground_truth_train, ground_truth_dev = ground_truth_shuffled[:dev_sample_index], ground_truth_shuffled[dev_sample_index:]

	rhyme_index_train = rhyme(source_sequence_train[:,0])
	rhyme_index_test = rhyme(source_sequence_dev[:,0])

	

	train_data = {'source_sequence':source_sequence_train,
	'target_sequence': target_sequence_train,
	'ground_truth':ground_truth_train,
	'length':[len(x) for x in source_sequence_train] }

	dev_data = {'source_sequence':source_sequence_dev,
	'target_sequence': target_sequence_dev,
	'ground_truth':ground_truth_dev,
	'rhyme_index':rhyme_index_test,
	'length':[len(x) for x in source_sequence_dev]}

	print("训练集/验证集 大小: {:d}/{:d}".format(len(target_sequence_train), len(target_sequence_dev,)))
	# print(len(rhyme_index_train))

	return train_data,dev_data


def texts2sequences(x):
	result = []
	for lines in x:
		line = []
		lines = lines.split(' ')
		for words in lines:
			if words in tokenizer.keys():
				line.append(tokenizer[words]) 
			else:
				line.append(tokenizer['<UNK>']) 
		result.append(line)
	return result


def data_iter(xfilename,yfilename):
	x = load_data(xfilename)
	y = load_data(yfilename)

	target_sequence = [yy + ' ' + token[2] for yy in y]
	ground_truth = [token[1] + ' ' + yy for yy in y]

	source_sequence = texts2sequences(x)
	target_sequence = texts2sequences(target_sequence)
	ground_truth = texts2sequences(ground_truth)


	source_sequence = tf.keras.preprocessing.sequence.pad_sequences(
		source_sequence,maxlen = 20, padding='post', truncating='post')
	target_sequence = tf.keras.preprocessing.sequence.pad_sequences(
		target_sequence,maxlen = 20, padding='post', truncating='post')
	ground_truth = tf.keras.preprocessing.sequence.pad_sequences(
		ground_truth,maxlen = 20, padding='post', truncating='post')


	train_data,dev_data = train_test_split(source_sequence,target_sequence,ground_truth)
	print("词表长度: {:d}".format(len(tokenizer) ))
	return train_data,dev_data


def rhyme(end_tokens):
	# 返回末尾词对应的押韵区间
	result = []
	for end_token in end_tokens:
		for i in range(len(index)):
			if index[i] <= end_token:
				end = index[i]
			else:
				start = index[i] - 1
				result.append([end,start])
				break
	return result

# batchGradient,Generates a batch iterator for a dataset.
def batch_iter(data, batch_size, num_epochs, shuffle=True):
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int((len(data)-1)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]



# xfilename = 'x_r.txt'
# yfilename = 'y_r.txt'
# train_data,dev_data = data_iter(xfilename,yfilename)
# print(train_data)
# print(dev_data)





